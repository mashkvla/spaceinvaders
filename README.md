# Space Invaders



## Popis zadání:

Hra Space Invaders je arkádová hra, ve které musí hráč před koncem hry získat co nejvyšší skóre.

Vytvořil jsem zjednodušenou verzi, kde má hráč 3 životy a protivníka, který po smrti zrychlí a objeví se na náhodném místě na hrací ploše. Cílem hry je získat co nejvíce bodů. Za každé zabití nepřítele získá hráč 10 bodů.

## Spuštění hry:

Hra je připravena ke spuštění ihned po kompilaci souboru CMake.
Doporučujeme hru spustit ve Visual Studiu, protože právě tam hra běží co nejplynuleji. V CLionu lze hru spustit, ale je téměř nemožné ji hrát, protože se hra změní na "slide show". 

Takže bude přiloženo video s ukázkou hraní.

## Ovládání programu:

A - posunutí lodi doprava.\
D - pohyb lodi doleva.\
Q - pro ukončení hry.\
F - pro střelbu.

## Příklad hry:
**Hlavní menu:**

Main Menu\
P: Play Game\
Q: Quit\
H: Instructions and Tips

Choose an option:

**Nápověda:**

The goal of the game is to score as many points as possible.\
For each enemy killed, the player receives 10 points.

A - move the ship to the right.\
D - move the ship to the left.\
Q - to end the game.\
F - for shooting.

HAVE FUN!

Press Q to go back to Main Menu: 

**Herní pole:**

```
######################
#                    #
#                    #
#                    #
#                    #
#                    #
#                    #
#                    #
#                  W #
#                    #
#         ,          #
#                    #
#         A          #
######################

Score: 30   Health: 3
```

## Testy:

Budu upřímný, je to arkádová hra s prvkem náhody. Jak ji správně otestovat a poskytnout návod na výhru, to nevím. Hlavní je, abyste si hru užili, v mém případě jsem si ji užil a přeji vám, abyste si ji užili také :)

Jediné, co mě napadlo, je, že když budete stát na místě, tak to samozřejmě prohrajete, po nějaké době.

Přeji hezký den a děkuju za pozornost!

S pozdravem,

Mashkin Vladimir 







